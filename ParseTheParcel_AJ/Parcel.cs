﻿using ParsetheParcel1;
using System;
using System.Collections.Generic;
using System.Text;

namespace ParseTheParcel
{
    public class Parcel : IParcel
    {
        private Outputs _outputs;
        private int Length { get; set; }
        private int Breadth { get; set; }
        private int Height { get; set; }
        private double Weight { get; set; }   
        double maxWeight = 25.0;
        private Dictionary<string, Dimensions> _parcelSpecs = new Dictionary<string, Dimensions>();
        
        public Parcel(int iLength, int iBreadth, int iHeight, double iWeight)
        {
            Length = iLength;
            Breadth = iBreadth;
            Height = iHeight;
            Weight = iWeight;
        }
        public void InitializeParcelSpecs()
        {
            _parcelSpecs.Add("Small", new Dimensions(200, 300, 150));
            _parcelSpecs.Add("Medium", new Dimensions(300, 400, 200));
            _parcelSpecs.Add("Large", new Dimensions(400, 600, 250));

        }
        private bool IsOverweight()
        {
            if (Weight > 0 && Weight <= maxWeight)
            { return false; }
            else
            return true;

        }
        public void CalculateParcelCost()
        {
            Outputs _outputs = new Outputs();
            try
            {
                _outputs.Cost = 0m; _outputs.PackageType = "invalid";
                if (!IsOverweight())
                {
                    InitializeParcelSpecs();
                    if(ValidateDimensions())
                    {
                        if (Length <= _parcelSpecs["Small"].Length &&
                                Breadth<=_parcelSpecs["Small"].Breadth &&
                            Height <=_parcelSpecs["Small"].Height)
                        {
                            _outputs.Cost = 5.00m;
                            _outputs.PackageType = "Small";
                        }
                        else if(Length <= _parcelSpecs["Medium"].Length &&
                                Breadth <= _parcelSpecs["Medium"].Breadth &&
                            Height <= _parcelSpecs["Medium"].Height)
                        {
                            _outputs.Cost = 7.50m;
                            _outputs.PackageType = "Medium";
                        }
                        else if(Length <= _parcelSpecs["Large"].Length &&
                                Breadth <= _parcelSpecs["Large"].Breadth &&
                            Height <= _parcelSpecs["Large"].Height)
                        {
                            _outputs.Cost = 8.50m;
                            _outputs.PackageType = "Large";
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            Console.WriteLine("Package Type: "+ _outputs.PackageType +" Total Cost :$"+_outputs.Cost);
            
        }
        private bool ValidateDimensions()
        {
            if (Length > 0 && Breadth > 0 && Height > 0)
            { return true; }
            else
            return false;

        }
      
    }
}
