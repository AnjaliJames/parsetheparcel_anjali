﻿using System;

namespace ParseTheParcel
{
    class Program
    {
        static void Main(string[] args)
        {
            int length;
            int breadth = 0;
            int height = 0;
            double weight = 0;
            Console.WriteLine(" Enter the length of the package and press enter");
             length = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(" Enter the breadth of the package and press enter");
             breadth = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(" Enter the height of the package and press enter");
             height = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(" Enter the weight of the package and press enter");
             weight =Convert.ToDouble(Console.ReadLine());
            //Console.ReadLine();
            Parcel _parcel = new Parcel(length,breadth,height,weight);
            _parcel.CalculateParcelCost();

        }
    }
}
