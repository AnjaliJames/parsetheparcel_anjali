﻿using System;

namespace ParsetheParcel1
{
    public interface IParcel
    {
        void CalculateParcelCost();
    }
    public struct Dimensions
    {
        public int Length { get; set; }

        public int Breadth { get; set; }

        public int Height { get; set; }

        public Dimensions(int iLength, int iBreadth, int iHeight)
        {
            Length = iLength;
            Breadth = iBreadth;
            Height = iHeight;
        }
    }

    public class Outputs
    {
        public string PackageType { get; set; }
        public decimal Cost { get; set; }
      
    }
}

